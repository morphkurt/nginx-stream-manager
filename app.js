var express = require('express')
var tools = require('./src/tools.js')
var app = express()
var bodyParser = require('body-parser')
var port = process.env.PORT || 8080
var key = process.env.KEY || 'abcdefg'
var netmask = require('netmask').Netmask
var morgan = require('morgan')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


// create "middleware"
if ( process.env.BUILDENV == 'PROD') {
app.use(morgan('combined'))
}


/* when request was received for on connect

1. extract the stream name
2. run the stream name through token validator
	a. check whether the stream name is valid
	b. stream is not expired
3. extract the client ip
4. use aws media live controller module (need to build it)
5. Start the available encoder pointing the input to rtmp end point
*/

app.post('/token/create', function (req,res,next) {
	var streamToken = JSON.stringify(req.body);
	try {
		var token= tools.create(streamToken,key)
		res.send({token: token});
	} catch(err) {
		next(err)
	}
})

app.post('/token/validate',function (req,res,next) {
	var streamToken = req.body;
	try {
		var token= tools.read(streamToken.token,key)
		res.send(token);
	} catch(err) {
		next(err)
	}
})

app.post('/endpoint/on_publish_done',function (req,res,next){
try {
	  var streamName = req.body.name
	  var requestIp = req.body.addr 
	  var token = tools.read(streamName,key)
  	  console.log('publishing of '+streamName+ ' ended')
	  res.send('OK')
  

} catch (e){
  	res.statusMessage = "Error happened"
  	res.status(400).end()
}
})



app.post('/endpoint/on_publish', function (req, res,next) {
  try {
  var streamName = req.body.name
  var requestIp = req.body.addr 
  var token = tools.read(streamName,key)
	// var payload = { startTime : 1556664620 , duration : 60 , streamId: 'ABCDEFGH', ips : [{ v4 : ip_buff, mask: 32}, { v4: ip_buff, mask: 0}]  };
	// check the token.ips
		var currentTime =  new Date() / 1000
		if (currentTime > token.startTime && token.duration == 0 && checkIP(token.ips,requestIp)) 
			res.send('OK')
		else if (currentTime > token.startTime + token.duration) {
			//stream token has been expired
		//	console.log("Token has been expired");
			res.statusMessage = "Token has been expired"
    		 	res.status(400).end()
		}
		else if ( currentTime < token.startTime) {
		//	console.log("Stream start time is in the future");
			res.statusMessage = "Stream startTime is in the future"
    		 	res.status(400).end()
		} 
		else if (!checkIP(token.ips,requestIp)){
		//	console.log("request ip is not within allowed range")
			res.statusMessage = "request ip is not within allowed range"
    		 	res.status(400).end()
		}
		else {
			res.send('OK')
		}
		// check streamName
		// check eventstart Time + duration < currentTime

} catch (e){
  	res.statusMessage = "Error happened"
  	res.status(400).end()
}
})


function checkIP(ipArray,requestIp) {
	var status = false
	ipArray.forEach( function (ip) {
		var block = new netmask(ip.v4+'/'+ip.mask)
		if (block.contains(requestIp)){
			status = true;
		}
	})
	return status
}

/*

when update request was received.

1. extract the stream id
2. store the stream id and the last time stamp
3. a clean up task needs to run in the background to identify stale streams and stop the encoder
*/

app.post('/endpoint/on_update', function (req, res,next) {

  try {
  var streamName = req.body.name
  var requestIp = req.body.addr 
  var token = tools.read(streamName,key)
	// var payload = { startTime : 1556664620 , duration : 60 , streamId: 'ABCDEFGH', ips : [{ v4 : ip_buff, mask: 32}, { v4: ip_buff, mask: 0}]  };
	// check the token.ips
		var currentTime =  new Date() / 1000
		if (currentTime > token.startTime && token.duration == 0 && checkIP(token.ips,requestIp)) 
			res.send('OK')
		else if (currentTime > token.startTime + token.duration) {
			//stream token has been expired
		//	console.log("Token has been expired");
			res.statusMessage = "Token has been expired"
    		 	res.status(400).end()
		}
		else if ( currentTime < token.startTime) {
		//	console.log("Stream start time is in the future");
			res.statusMessage = "Stream startTime is in the future"
    		 	res.status(400).end()
		} 
		else if (!checkIP(token.ips,requestIp)){
		//	console.log("request ip is not within allowed range")
			res.statusMessage = "request ip is not within allowed range"
    		 	res.status(400).end()
		}
		else {
			res.send('OK')
		}
		// check streamName
		// check eventstart Time + duration < currentTime

} catch (e){
  	res.statusMessage = "Error happened"
  	res.status(400).end()
}

})

app.use(function (err, req, res, next) {
  res.status(400).send('something was not right')
})

app.listen(port, () => console.log(`nginx stream manager app listening on port ${port}!`))

module.exports = app

