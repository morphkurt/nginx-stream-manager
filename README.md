

# To do

* [X]  Build a basic node express wrapper
* [X]  Log requests from nginx rtmp module
* [X]  Test simple passthrough usercase to allow all traffic to go through
* [X]  Integrate the rtmp authenticate module
* [ ]  Evaluate AWS Media Live API to control
* [ ]  Make Docker Based distribution

# On this Page

* [nginx rtmp stream manager](#nginx rtmp stream manager)
* [Flow diagram](#flow_diagram)
* [RTMP stream token](#rtmp_stream_token)
* [To do](#to_do)
* [How To](#how_to)

# nginx rtmp stream manager

Node based stream controller to manage [nginx rtmp module](https://github.com/arut/nginx-rtmp-module)

# Flow Diagram

```mermaid
sequenceDiagram
    producer->>nginx: rtmp://<elb>:1935/live/<token>
    nginx->>stream manager: http://x.x.x.x/endpoint/on_publish
    loop check_token
        stream manager->>stream manager: check token
    end
    stream manager->>nginx: (status 200 || status 400)
    stream manager->>medialive: (start channel)
    medialive->>nginx: rtmp://vpc_ip:1935/live/<token>
    medialive->>mediastore: arn://mediastore
    cloudfront->>mediastore: http
    telstra CDN->>cloudfront: http
    customer->>telstra CDN: https://example.ngcdn.telstra.com/foo/baa.m3u8
    nginx->>stream manager: http://x.x.x.x/endpoint/on_update
    loop check_token
        stream manager->>stream manager: check currentTime < startTime+duration
        
    end
    nginx->>stream manager: http://x.x.x.x/endpoint/on_done
    stream manager->>medialive: (stop channel)
```
# RTMP stream token

## Design principles

* The RTMP endpoint needs to be protected from unauthorised streaming attempts.
* The IP based blocking, requires manual resources.
* Username and password method needs some sort of database to manage credentials
* The RTMP authentication method should be supported by all stream publishing software.
* Ability provide timebased access.

## Solution

```mermaid
graph TD
A[startTime]
B[duration]
C[allowedIP]
D[streamName]
A-->E[protobuf]
B-->E[protobuf]
C-->E[protobuf]
D-->E[protobuf]
E-->|AES-192-GCM|F[encryptedProtobuf] 
F-->I
G[IV]-->I[binary token]
H[AUTHTAG]-->I
I-->|base58|j[token]
```
>>>
* protocol buffer used to efficiently compact data
* the encrypted token will be shared with the content provider with the streaming endpoint
* at the time of the rtmp connection the token shall be passed to stream manager
* stream manager will decrypt the token to extract the given fields and checks whether the connections has.
    1. allowed within the given time
    2. coming from correct ip / ip range
* Random IV is used
* Auth tag is used to ensure authenticity of the token
* base58 used for human/copy paste friendly tokens. (inspired by bitcoin)
>>>

## Protocol Buffer Schema

```protobuf
syntax = "proto3";
message token {
  int32 start_time = 1;
  int32 duration = 2;  
  string stream_id = 3;
  repeated ip_address ips = 4;
}
message ip_address {
   oneof ip {  
     fixed32 v4 = 1;
     bytes v6 = 2 ;
   }
   int32 mask = 3;
}
```


# How to

## How to create the token

```bash
curl -X POST \
  http://<hostname>/token/create \
  -H 'content-type: application/json' \
  -d '{
    "startTime": 123214221,
    "duration": 60,
    "streamId": "ABCDEFGHJKJKHKHKHKHJKHKHK",
    "ips": [
        {
            "v4": "255.168.0.255",
            "mask": 32
        }
    ]
}'
```

Output Should be

```json
Output: 
{
    "token": "5tQSqzNyg1dQmJCwbbakneP1AQD41AobAuhSjZfHPXgZ6fZoUfMv6SqTHHCMP8iHxM"
}
```

## How to validate the token

```bash
curl -X POST \
  http://localhost:8080/token/validate \
  -H 'Accept: */*' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Host: localhost:8080' \
  -H 'Postman-Token: d99027d2-b1da-410d-ab28-0462ea6c32e0,61285d1c-e6e7-4800-9fc7-96490dacbbf1' \
  -H 'User-Agent: PostmanRuntime/7.11.0' \
  -H 'accept-encoding: gzip, deflate' \
  -H 'cache-control: no-cache' \
  -H 'content-length: 85' \
  -H 'content-type: application/json' \
  -d '{
    "token": "5tQSqzNyg1dQmJCwbbakneP1AQD41AobAuhSjZfHPXgZ6fZoUfMv6SqTHHCMP8iHxM"
}'
```

Output should be
```json
{
    "startTime": 123214221,
    "duration": 60,
    "streamId": "ABCDEFGHJKJKHKHKHKHJKHKHK",
    "ips": [
        {
            "v4": "255.168.0.255",
            "mask": 32
        }
    ]
}
```

## On publish call

> http://hostname/endpoint/on_publish

1. extract the stream name
2. run the stream name through token validator
	a. check whether the stream name is valid
	b. stream is not expired
3. extract the client ip
4. use aws media live controller module (need to build it)
5. Start the available encoder pointing the input to rtmp end point

## On update call

> http://hostname/endpoint/on_update

Every X seconds nginx server will call the above URL

