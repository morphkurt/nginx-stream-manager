const protobuf = require('protobufjs')
const bs58 = require('bs58')
const crypto = require('crypto');
const algorithm = 'aes-192-gcm';
const fs = require('fs');
const proto = fs.readFileSync('src/token.proto', 'utf8');



module.exports = {	
	/* creates the encrypted token from the token object with the key file provided */
	create: function (clearToken,key) {
		let token = JSON.parse(clearToken)
		
		//convert the string ip address to int32
		token.ips.forEach( function (ip) {
			ip.v4 = ipToByte(ip.v4);
		})
		return encode(token,key)	
		
	},

	
	/*
	Place to decode the encryped token
	*/	
	read: function (encToken,key) {
		
		return decode(encToken,key)
	}
	
}

/*
# Roughly the Process
	-	Outcome: Token which is small enough to be passed around and copy and paste
	-	Convert the json objec to protocol buffer (token.proto)
	-	Encrypt the output buffer using aes-192-gcm
	-	1 byte IV
	-	4 byte authtag 
	-	token = encryped message + IV + authtag
	-	Finaly base58 encoding the buffer to avoid funny characters
*/

function decode(encToken,key){
	try {
		var decrypted_message = decrypt(encToken,key)
	}
	catch(e) {
		throw (e)
	}
	var root = protobuf.parse(proto).root
	let token = root.lookupType("token")
	message = token.decode(decrypted_message);
	message.ips.forEach( function (obj) {
		obj.v4 = byteToIp(obj.v4)
	})
	return message
 
}

function encode(tok,password) {
	var root = protobuf.parse(proto).root
	let token = root.lookupType("token")
	let errMsg = token.verify(tok)
	if (errMsg)
		throw (errMsg)
	let message = token.create(tok) 
	let buffer =token.encode(message).finish();
	const key = crypto.scryptSync(password, 'salt', 24);
	// Use `crypto.randomBytes` to generate a random iv instead of the static iv
	// shown here.
	const iv = crypto.randomBytes(1);; // Initialization vector.

	const cipher = crypto.createCipheriv(algorithm, key, iv);
		
	let encrypted = Buffer.concat([cipher.update(buffer),cipher.final()]);	
		
	let tag = cipher.getAuthTag().slice(0,4)

	let endToken = Buffer.concat([encrypted, iv, tag])
		
	return bs58.encode(endToken)
}

/*
# Roughly the Process (Same as the encoding process but in reverse)
*/



function decrypt(encrypted_token,password){
	const binary_token = bs58.decode(encrypted_token)
	const authtag = binary_token.slice(binary_token.length-4,binary_token.length);
	const iv = binary_token.slice(binary_token.length-5,binary_token.length-4);
	const encrypted_potion = binary_token.slice(0,binary_token.length-5);
	const key = crypto.scryptSync(password, 'salt', 24);
	const decipher = crypto.createDecipheriv(algorithm, key, iv);
	decipher.setAuthTag(authtag)
	try{
		let decrypted = Buffer.concat([decipher.update(encrypted_potion),decipher.final()]);
		return decrypted	
	} catch {
		throw ('unable to decrypt')
	} 
}

/*
	Convert v4 ip to 4 byte int
*/

function ipToByte(ip){
	return new Buffer.from(ip.split('.')).readInt32BE(0)
}

/*
	Reverse the 4 byte to x.x.x.x ip address
*/

function byteToIp(ipBytes){
	let buffer = Buffer.alloc(4)
	buffer.writeUInt32BE(ipBytes, 0);
	return buffer.readUIntBE(0, 1)+'.'+buffer.readUIntBE(1,1)+'.'+buffer.readUIntBE(2,1)+'.'+buffer.readUIntBE(3,1)
}	


