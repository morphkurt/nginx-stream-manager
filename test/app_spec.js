var chai = require('chai');
var chaiHttp = require('chai-http');
var app = require('../app');

var expect = chai.expect;

chai.use(chaiHttp);

describe('nginx stream manager', function() {
     describe('/endpoint/on_update', function() {
        it('Given valid nginx rtmp request with expired token, it should respond with 400', function(done) {
            chai.request(app)
                .post('/endpoint/on_update')
		.type('form')
		.send({
		    'name': '5dmEDeZMJrGGMtRKyZ73sUNNk8uaue7vJvjnHncdzevJW',
		    'addr': '172.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(400);
                    done();
                });
        });
	 it('Given valid nginx rtmp request with start time in the past but duration = 0 , it should respond with 200', function(done) {
            chai.request(app)
                .post('/endpoint/on_update')
		.type('form')
		.send({
		    'name': 'C6f4dMEkZACwfydwQxJtLwJduhiyKWnNoKBxAJT5DpLG',
		    'addr': '172.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(200);
                    done();
                });
        });
	it('Given valid nginx rtmp request with start time in the past but duration = 0 and invalid ip range, it should respond with 400', function(done) {
            chai.request(app)
                .post('/endpoint/on_update')
		.type('form')
		.send({
		    'name': 'C6f4dMEkZACwfydwQxJtLwJduhiyKWnNoKBxAJT5DpLG',
		    'addr': '142.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(400);
                    done();
                });
        });

	it('Given valid nginx rtmp request with stream start time in the future, it should respond with 400', function(done) {
            chai.request(app)
                .post('/endpoint/on_update')
		.type('form')
		.send({
		    'name': '5rAExDHtqTAbnv23fKdz8SSALvVzB2VyVU4RuL4NrmcjF',
		    'addr': '172.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(400);
                    done();
                });
        });
	it('Given valid nginx rtmp request with stream start time in back but current event, it should respond with 200', function(done) {
            chai.request(app)
                .post('/endpoint/on_update')
		.type('form')
		.send({
		    'name': '2BM1ZHCE8SqWsWAbgrkr4h5y7JfnG4B6zHLHCDF3Cooxq8KZyN',
		    'addr': '172.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(200);
                    done();
                });
        });
	it('Given valid nginx rtmp request with current event but invalid ip, it should respond with 400', function(done) {
            chai.request(app)
                .post('/endpoint/on_update')
		.type('form')
		.send({
		    'name': 't4gUEz89BY7wbDL2fd8T3FQPGZ8xMD3okiw57HtKD2H19G725',
		    'addr': '172.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(400);
                    done();
                });
        });
	it('Given valid nginx rtmp request with current event 0.0.0.0/0, it should respond with 200', function(done) {
            chai.request(app)
                .post('/endpoint/on_update')
		.type('form')
		.send({
		    'name': 'wcAQUhTiK8unzHuV15LwqjLX48LeXNBRE4BbnhVcSMWcdCLn',
		    'addr': '172.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(200);
                    done();
                });
        });
	it('Given valid nginx rtmp request with multi ip ranges but at least one valid range, it should respond with 200', function(done) {
            chai.request(app)
                .post('/endpoint/on_update')
		.type('form')
		.send({
		    'name': 'nL64Y3h2YxodGvqDaSWokwfjkn7ZpJ7rs5D9CiTC2qja2n33mMh83Zj',
		    'addr': '172.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(200);
                    done();
                });
        });
	it('Given valid nginx rtmp request with multi ip ranges but at least all invalid range, it should respond with 400', function(done) {
            chai.request(app)
                .post('/endpoint/on_update')
		.type('form')
		.send({
		    'name': 'nL64Y3h2YxodGvqDaSWokwfjkn7ZpJ7rs5D9CiTC2qja2n33mMh83Zj',
		    'addr': '112.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(400);
                    done();
                });
        });



    });



     describe('/endpoint/on_publish', function() {
        it('Given valid nginx rtmp request with expired token, it should respond with 400', function(done) {
            chai.request(app)
                .post('/endpoint/on_publish')
		.type('form')
		.send({
		    'name': '5dmEDeZMJrGGMtRKyZ73sUNNk8uaue7vJvjnHncdzevJW',
		    'addr': '172.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(400);
                    done();
                });
        });
	 it('Given valid nginx rtmp request with start time in the past but duration = 0 , it should respond with 200', function(done) {
            chai.request(app)
                .post('/endpoint/on_publish')
		.type('form')
		.send({
		    'name': 'C6f4dMEkZACwfydwQxJtLwJduhiyKWnNoKBxAJT5DpLG',
		    'addr': '172.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(200);
                    done();
                });
        });
	it('Given valid nginx rtmp request with start time in the past but duration = 0 and invalid ip range, it should respond with 400', function(done) {
            chai.request(app)
                .post('/endpoint/on_publish')
		.type('form')
		.send({
		    'name': 'C6f4dMEkZACwfydwQxJtLwJduhiyKWnNoKBxAJT5DpLG',
		    'addr': '142.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(400);
                    done();
                });
        });

	it('Given valid nginx rtmp request with stream start time in the future, it should respond with 400', function(done) {
            chai.request(app)
                .post('/endpoint/on_publish')
		.type('form')
		.send({
		    'name': '5rAExDHtqTAbnv23fKdz8SSALvVzB2VyVU4RuL4NrmcjF',
		    'addr': '172.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(400);
                    done();
                });
        });
	it('Given valid nginx rtmp request with stream start time in back but current event, it should respond with 200', function(done) {
            chai.request(app)
                .post('/endpoint/on_publish')
		.type('form')
		.send({
		    'name': '2BM1ZHCE8SqWsWAbgrkr4h5y7JfnG4B6zHLHCDF3Cooxq8KZyN',
		    'addr': '172.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(200);
                    done();
                });
        });
	it('Given valid nginx rtmp request with current event but invalid ip, it should respond with 400', function(done) {
            chai.request(app)
                .post('/endpoint/on_publish')
		.type('form')
		.send({
		    'name': 't4gUEz89BY7wbDL2fd8T3FQPGZ8xMD3okiw57HtKD2H19G725',
		    'addr': '172.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(400);
                    done();
                });
        });
	it('Given valid nginx rtmp request with current event 0.0.0.0/0, it should respond with 200', function(done) {
            chai.request(app)
                .post('/endpoint/on_publish')
		.type('form')
		.send({
		    'name': 'wcAQUhTiK8unzHuV15LwqjLX48LeXNBRE4BbnhVcSMWcdCLn',
		    'addr': '172.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(200);
                    done();
                });
        });
	it('Given valid nginx rtmp request with multi ip ranges but at least one valid range, it should respond with 200', function(done) {
            chai.request(app)
                .post('/endpoint/on_publish')
		.type('form')
		.send({
		    'name': 'nL64Y3h2YxodGvqDaSWokwfjkn7ZpJ7rs5D9CiTC2qja2n33mMh83Zj',
		    'addr': '172.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(200);
                    done();
                });
        });
	it('Given valid nginx rtmp request with multi ip ranges but at least all invalid range, it should respond with 400', function(done) {
            chai.request(app)
                .post('/endpoint/on_publish')
		.type('form')
		.send({
		    'name': 'nL64Y3h2YxodGvqDaSWokwfjkn7ZpJ7rs5D9CiTC2qja2n33mMh83Zj',
		    'addr': '112.17.0.1',
		  })
                .end(function(err, res) {
                    expect(res).to.have.status(400);
                    done();
                });
        });




    });
    describe('/token/validate', function() {
        it('Given a invalid token, it should respond with 400', function(done) {
            chai.request(app)
                .post('/token/validate')
                .set('content-type', 'application/json')
                .send({
                    token: "5tiyQSqzNyg1dQmJCwbbakneP1AQD41AobAuhSjZfHPXgZ6fZoUfMv6SqTHHCMP8iHxM"
                })
                .end(function(err, res) {
                    expect(res).to.have.status(400);
                    done();
                });
        });
	it('Given a valid token, it should respond with 200', function(done) {
            chai.request(app)
                .post('/token/validate')
                .set('content-type', 'application/json')
                .send({
                    token: "5tQSqzNyg1dQmJCwbbakneP1AQD41AobAuhSjZfHPXgZ6fZoUfMv6SqTHHCMP8iHxM"
                })
                .end(function(err, res) {
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('startTime');
                    expect(res.body).to.have.property('duration');
                    expect(res.body).to.have.property('streamId');
                    done();
                });
        });

    });
    describe('/token/create', function() {
        it('Given a valid token json object, it should valid encrypted token', function(done) {
            chai.request(app)
                .post('/token/create')
                .set('content-type', 'application/json')
                .send(JSON.parse('{"startTime": 1557358103,"duration": 200,"streamId": "ABCDEFGH","ips": [{"v4": "172.17.0.1","mask": 32}]}'))
                .end(function(err, res) {
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('token');
                    done();
                });
        });
    });
});
